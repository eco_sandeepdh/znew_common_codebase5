<?php
include 'db.php';
$client_id =$_POST['client_id'];
$con = new Connection($DB_NAMES_ARRAY[$client_id]);
include 'wsJSON.php';
$JSONVar = new wsJSON($con);
$salespersonid	= $_POST['userid'];
$store_name		= fnEncodeString($_POST['store_name']);
$owner_name		= fnEncodeString($_POST['owner_name']);
$contact_no		= $_POST['contact_no'];
$store_Address	= fnEncodeString($_POST['store_Address']);
$lat			= $_POST['lat'];
$lng			= $_POST['lon'];
$city			= $_POST['city'];
$state			= $_POST['state'];

$distributorid	= $_POST['distributorid'];
$suburbid		= $_POST['suburbid'];
$subarea_id		= $_POST['subareaid'];

//new fields added
$billing_name	= $_POST['billing_name'];
$bank_acc_name		= $_POST['bank_acc_name'];
$bank_acc_no		= $_POST['bank_acc_no'];
$bank_name	= $_POST['bank_name'];
$bank_b_name		= $_POST['bank_b_name'];
$bank_ifsc		= $_POST['bank_ifsc'];
$gst_number = $_POST['gst_number'];


$jsonOutput 	= $JSONVar->fnAddNewStore($salespersonid,$store_name,$owner_name,$contact_no,$store_Address,$lat,$lng,$city,$state,$distributorid,$suburbid,$subarea_id,$billing_name,$bank_acc_name,$bank_acc_no,$bank_name,$bank_b_name,$bank_ifsc,$gst_number);
echo $jsonOutput;
