<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/orderManage.php";
$orderObj = new orderManage($con, $conmain);
$report_title = $orderObj->getReportTitle();
$row = $orderObj->getSPattendance();

$record_count = mysqli_num_rows($row);
//echo "sdfsd<pre>";print_r($row);
$colspan = "9";
?>
<?php if ($_POST["actionType"] == "excel") { ?>
    <style>table { border-collapse: collapse; } 
        table, th, td {  border: 1px solid black; } 
        body { font-family: "Open Sans", sans-serif; 
               background-color:#fff;
               font-size: 11px;
               direction: ltr;}
        </style>
    <?php }
    ?>

    <table 
        class="table table-striped table-bordered table-hover table-highlight table-checkable" 
    data-provide="datatable" 
    data-display-rows="10"
    data-info="true"
    data-search="true"
    data-length-change="true"
    data-paginate="true"
    id="sample_5">
    <thead>
        <tr>
            <td colspan="<?= $colspan; ?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b><?php if (!empty($report_title))
        echo $report_title;
    else
        echo "SP Attendance Report All";
    ?></b></h4></td>              
        </tr>
        <tr>
            <th data-filterable="false" data-sortable="true" data-direction="desc">SR NO.</th>
            <th data-filterable="false" data-sortable="true" data-direction="desc">Name</th>
            <th data-filterable="false" data-sortable="false" data-direction="desc">Date</th>
            <th data-filterable="false" data-sortable="false" data-direction="desc">Start Day</th>	
            <th data-filterable="false" data-sortable="true" data-direction="desc">End Day </th>   
            <th data-filterable="false" data-sortable="false" data-direction="desc">Working Hours</th>	
            <th data-filterable="false" data-sortable="false" data-direction="desc">Distance Travelled</th>
            <!-- <th data-filterable="false" data-sortable="true" data-direction="desc">Present Status</th>	 -->
            <th data-filterable="false" data-sortable="true" data-direction="desc">Attendance</th>
            <th data-filterable="false" data-sortable="true" data-direction="desc">Remarks</th>              
        </tr>
    </thead>
    <tbody>					
        <?php
        if (!empty($row)) {
            foreach ($row as $key => $value) {
                $leavedt = $value['leavedt'];
                ?>
                <tr class="odd gradeX">				
                    <td align='right'><?= $key + 1; ?></td>
                    <td align='Left'><?= $value['firstname']; ?></td>
                    <td align='right'>                           
        <?= date('d-m-Y', strtotime($value['tdate'])); ?>                           	
                    </td>
                    <td align='right'><?= date('H:i:s', strtotime($value['tdate'])); ?></td>
                    <td align='right'><?= date('H:i:s', strtotime($value['dayendtime'])); ?></td>
                    <td align='right'><!-- <?= $value['hours_difference'] ?> --> <?= number_format((float) $value['hours_difference'], 0, '.', '') ?> </td>
                    <td align='Left'><?= number_format((float) $value['todays_travelled_distance'], 3, '.', '') ?></td>
                    <td>
                        <?php if ($value['presenty'] == '1') { ?>
                            <b>P</b>
        <?php } else {
            ?>
                            <b>A</b> 
                            <?php  } ?>
                        </td> 
                        <td align='right'> -							
                        </td>
                    </tr>
                <?php } ?>
                <?php
            }/* else{
              echo "<tr><td colspan='5' align='center'>No matching records found</td></tr>";
              } */
            if ($_POST["actionType"] == "excel" && $row == 0) {
                echo "<tr><td>No matching records found</td></tr>";
            }
            ?>	

        </tbody>	
    </table>



    <script>
        jQuery(document).ready(function () {

            ComponentsPickers.init();
            TableManaged.init();
        });


        $(document).ready(function () {
            var table = $('#sample_5').dataTable();
            // Perform a filter
            table.fnFilter('');
            // Remove all filtering
            //table.fnFilterClear();

        });
    </script> 
    <!-- END JAVASCRIPTS -->
    <?php
    if ($_POST["actionType"] == "excel") {
        if ($row != 0) {
            header("Content-Type: application/vnd.ms-excel");
            header("Content-disposition: attachment; filename=SP_Summary_Report.xls");
        }
    }
    ?>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->