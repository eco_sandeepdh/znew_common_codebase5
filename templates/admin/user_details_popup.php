<?php 
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/userManage.php";   
$userObj    =   new userManager($con,$conmain);

$user_details_id = $_POST['user_details_id'];
$user_type = $_POST['user_type'];
$user_details = $userObj->getAllUsersViewAllDetails($user_details_id);
//print"<pre>";print_r($user_details);
?>
<div class="modal-header">

<button type="button" name="btnPrint" id="btnPrint" class="btn btn-primary" >Test</button>


<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel"></h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divOrderPrintArea">
<div class="row">
<div class="col-md-12">   
	<div class="portlet box blue-steel">
		<div class="portlet-title ">
			<div class="caption printHeading">
				<?=$user_type;?>
			</div>                          
		</div>
		<div class="portlet-body">
			<table class="table table-striped table-bordered table-hover" id="sample_2" width="100%">
			<tr>
				<td>Name</td>
				<td><?=$user_details['firstname'];?></td>				
			</tr>
			<tr>
				<td>Email</td>
				<td><?=$user_details['email'];?></td>				
			</tr>
		    <tr>
				<td>Mobile No.</td>
				<td><?=$user_details['mobile'];?></td>				
			</tr>
			<tr>
				<td>Subarea</td>
				<td><?=$user_details['subarea_name'];?></td>				
			</tr>
			<tr>
				<td>Taluka</td>
				<td><?=$user_details['suburbnm'];?></td>				
			</tr>
			
			<tr>
				<td>City</td>
				<td><?=$user_details['cityname'];?></td>				
			</tr>
			<tr>
				<td>State</td>
				<td><?=$user_details['statename'];?></td>				
			</tr>
			<tr>
				<td>Office Phone No</td>
				<td><?=$user_details['office_phone_no'];?></td>				
			</tr>
			<tr>
				<td><b>Other Details</b></td>
				<td></td>				
			</tr>				
			
			<tr>
				<td>Account Name</td>
				<td><?=$user_details['accname'];?></td>				
			</tr>
			<tr>
				<td>Account No</td>
				<td><?=$user_details['accno'];?></td>				
			</tr>
		    <tr>
				<td>Branch</td>
				<td><?=$user_details['accbrnm'];?></td>				
			</tr>
			<tr>
				<td>Bank Name</td>
				<td><?=$user_details['bank_name'];?></td>				
			</tr>
			<tr>
				<td>IFSC Code</td>
				<td><?=$user_details['accifsc'];?></td>				
			</tr>
			<tr>
				<td>GST Number</td>
				<td><?=$user_details['gst_number_sss'];?></td>				
			</tr>

			</table>
</div>
</div>
</div>
</div>
</div>