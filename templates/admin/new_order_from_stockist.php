<?php
include "../includes/grid_header.php";
include "../includes/commonManage.php";
$commonObj = new commonManage($con, $conmain);
$commonObjctype = $commonObj->log_get_commonclienttype($con, $conmain);

$user_type = $_SESSION[SESSION_PREFIX . 'user_type'];
$user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "Orders";
        $activeMenu = "Ordersnewstockist";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- /.modal -->

                <h3 class="page-title">
                    New Orders
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">New Orders</a>
                        </li>
                    </ul>

                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">

                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">Shopping Cart </div>	
                                <a id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" onClick="cartActionEmpty();">Empty Cart</a>
                                <div class="clearfix"></div>
                            </div>						
                            <div class="portlet-body">	
                                
                                    <table class="table table-striped table-bordered table-hover" id="myTable" >
                                        <thead>
                                            <tr>					
                                                <th>
                                                    Category
                                                </th>
                                                <th>
                                                    Product Name
                                                </th>
                                                <th>
                                                    Product HSN
                                                </th>
                                                <th>
                                                    Product Price
                                                </th>
                                                <th>
                                                    Product Variant
                                                </th>
                                                <th>
                                                    Product Image
                                                </th>
                                                <th>
                                                    Quantity
                                                </th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody >

                                        </tbody>
                                    </table>
                                 <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" >
                                     <div class="form-group" id="place_order_div" style="display:none">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="button" name="place_order_btnsubmit" id="place_order_btnsubmit" class="btn btn-primary" onclick="placeOrderStockist();">Place Order</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                 </form>
                            </div>
                            <!-- END PAGE CONTENT-->
                        </div>
                        <div class="clearfix"></div>   


                        <div class="portlet box blue-steel">
                            <div class="portlet-title">

                                <div class="caption">Product Listing</div>
                                <a id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" onclick="cartActionAdd();">Add Order</a>
                                <div class="clearfix"></div>

                            </div>

                            <div class="portlet-body">
                                <form id="frmCart">
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>					
                                                <th>
                                                    Category
                                                </th>
                                                <th>
                                                    Product Name
                                                </th>
                                                <th>
                                                    Product HSN
                                                </th>
                                                <th>
                                                    Product Price
                                                </th>
                                                <th>
                                                    Product Variant
                                                </th>
                                                <th>
                                                    Product Image
                                                </th>
                                                <th>
                                                    Quantity
                                                </th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql = "SELECT a.categorynm,b.productname,b.id,pv.producthsn,pv.price,pv.variant_1,pv.variant_2,pv.id as pvid, "
                                                    . " pv.productimage,b.added_by_userid,b.added_by_usertype, a.id AS cat_id  "
                                                    . " FROM tbl_category a,tbl_product b "
                                                    . " left join tbl_product_variant pv on b.id = pv.productid "
                                                    . " where a.id=b.catid AND b.isdeleted != 1 "
                                                    . " and ((b.added_by_userid=0) OR (b.added_by_userid=1) OR (b.added_by_userid='$user_id'))";
                                            $result = mysqli_query($con, $sql);
                                            $row_count = mysqli_num_rows($result);
                                            while ($row = mysqli_fetch_array($result)) { //echo "<pre>";print_r($row);
                                                $prodcode = $row['id'] . '_' . $row['pvid'];
                                                ?>

                                                <tr class="odd gradeX" id="<?php echo $prodcode; ?>">
                                                    <td id="num1<?php echo $prodcode; ?>">
                                                        <?php echo fnStringToHTML($row['categorynm']); ?>
                                                    </td>
                                                    <td id="num2<?php echo $prodcode; ?>">
                                                        <?php echo fnStringToHTML($row['productname']); ?>
                                                    </td>
                                                    <td id="num3<?php echo $prodcode; ?>">
                                                        <?php echo fnStringToHTML($row['producthsn']); ?>
                                                    </td>
                                                    <td id="num4<?php echo $prodcode; ?>">
                                                        <?php echo fnStringToHTML($row['price']); ?>
                                                    </td>										
                                                    <td id="num5<?php echo $prodcode; ?>">
                                                        <?php
                                                        $imp_variant1 = explode(',', $row['variant_1']);
                                                        $var_unit_id = trim($row['variant_1'], ",");
                                                        $sqlvarunit = "SELECT unitname FROM tbl_units  where id='" . $imp_variant1[1] . "'";
                                                        $resultvarunit = mysqli_query($con, $sqlvarunit);
                                                        while ($rowvarunit = mysqli_fetch_array($resultvarunit)) {
                                                            if ($imp_variant1[0] != '' && $commonObjctype != '1') {
                                                                echo $imp_variant1[0] . "-" . $rowvarunit['unitname'] . " ";
                                                            }
                                                        }
                                                        $imp_variant2 = explode(',', $row['variant_2']);
                                                        //echo "123  <pre>";print_r($imp_variant1);
                                                        $var_unit_id = trim($row['variant_2'], ",");
                                                        $sqlvarunit = "SELECT unitname FROM tbl_units  where id='" . $imp_variant2[1] . "'";
                                                        $resultvarunit = mysqli_query($con, $sqlvarunit);
                                                        while ($rowvarunit = mysqli_fetch_array($resultvarunit)) {
                                                            if ($imp_variant2[0] != '' && $commonObjctype != '1') {
                                                                echo $imp_variant2[0] . "-" . $rowvarunit['unitname'] . "  ";
                                                            } else {
                                                                echo "-" . $rowvarunit['unitname'] . "  ";
                                                            }
                                                        }
                                                        echo "<br>";
                                                        ?>

                                                    </td>
                                                    <td id="num6<?php echo $prodcode; ?>">
                                                        <?php if (!empty($row['productimage'])) { ?>
                                                            <img src="upload/<?php echo $row['productimage']; ?>" 
                                                                 alt=<?php echo $row['productimage']; ?> 
                                                                 width="100px" />
                                                             <?php } ?>										 
                                                    </td>
                                                    <td id="num7<?php echo $prodcode; ?>">
                                                        <input type="number"  name="prodqnty" id="qty_<?php echo $prodcode; ?>" 
                                                               value="0" size="2" style="width: 55px; margin: 5px 5px 5px 5px;"> 
                                                    </td>
                                                </tr>
                                            <?php } ?>							
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div> 

                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php include "../includes/grid_footer.php" ?>
    <!-- END FOOTER -->

    <script>
        function placeOrderStockist(){           
            var orderarray=[];
            $('#myTable tbody tr').each(function () {  
                var void1 = [];
                var newenergy = this.id.replace('mytabletr', '');
                var tempstrip=$('#myqty_' + newenergy + '').html();                            
                void1.push(tempstrip.replace(/ +$/, "")); 
                void1.push(newenergy); 
                 orderarray.push(void1); 
            });
          
            if (orderarray.length > 0) {
                var url = 'updateOrderFromStockist.php';               
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {data123 : orderarray},
                    error: function (data) {
                        console.log("error:" + data)
                    },
                    success: function (data) {
                        console.log(data);
                        if (data > 0) {
                            alert("Order Placed Successfully");
                            location.reload();
                        } else {
                            alert("Not updated");
                        }
                    }
                });
            } else {
                alert("Please add some product!");
                return false;
            }
        }
        function cartActionEmpty(){
            $("#myTable tbody").empty();
        }
        function cartActionEmptyItem(trid){
            $("#mytabletr"+trid+"").remove();
        }

        function cartActionAdd() {
            var void1 = [];
            var void2 = [];
            $('input[name="prodqnty"]').each(function () {
                if (this.value > 0) {
                    void1.push(this.value);
                    void2.push(this.id);
                }
            });
            var energy = void1.join();
            
            if (energy != '') {
                Object.keys(void1).forEach(function (key) {
                    var newenergy = void2[key].replace('qty_', '#');
                    var newenergy1 = newenergy.replace('#', '');
                    if ($('#mytabletr' + newenergy1 + '').length) {
                        var sumvalue = +$('#myqty_' + newenergy1 + '').html() + +void1[key];
                        console.log(sumvalue);
                        $('#myqty_' + newenergy1 + '').html('' + sumvalue + '');
                    } else {
                       
                        $("#myTable").append("<tr class='odd gradeX' id='mytabletr" + newenergy1 + "'>\
                            <td>" + $('#num1' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num2' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num3' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num4' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num5' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num6' + newenergy1 + '').html() + "</td>\
                            <td ><button type='button' class='btn btn-primary' onClick='cartActionEmptyItem(\"" + newenergy1 + "\");'>Remove <span id='myqty_" + newenergy1 + "' class='badge'>"+$('#qty_' + newenergy1 + '').val()+"</span></button></td>\
                        </tr>");
                                
                    }

                });
                if ($('#myTable tr').length>0) {                    
                    $('#place_order_div').show();
                }
                $('input[name="prodqnty"]').each(function () {
                    if (this.value > 0) {
                        $('input[name="prodqnty"]').val('0');
                    }
                });
            } else {
                alert("Please add some quantity!");
            }
        }
    </script>


</body>
<!-- END BODY -->
</html>