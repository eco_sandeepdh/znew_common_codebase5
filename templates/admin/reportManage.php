<?php
/***********************************************************
 * File Name	: reportManage.php
 ************************************************************/	
class reportManage
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
	}
	public function get_all_sp_leave() 
	{		
		$today = date('Y-m-d');
		if($today <= date('Y-m-15')){
			$frmdate = date("Y-m-01");
			$todate  = date("Y-m-15");
		}else{
			$frmdate = date("Y-m-16");
			$todate  =  date("Y-m-d", strtotime("last day of this month"));
		}
		
		 $sql = "SELECT tsl.tdate, tsl.sp_id,u.firstname,tsl.reason FROM tbl_sp_attendance  tsl 
		left join tbl_user u on tsl.sp_id=u.id 
		where presenty !='1' and 
		(date_format(tdate, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%Y-%m-%d') AND date_format(tdate, '%Y-%m-%d') <= STR_TO_DATE('".$todate."','%Y-%m-%d'))";
		$result1 = mysqli_query($this->local_connection,$sql);		
		$i = 0;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
        public function get_all_sp_current_location() 
	{		
            $today = date('Y-m-d');

            $sql = "SELECT tsl.id, tsl.userid, tsl.lattitude, tsl.longitude, tsl.tdate, tsl.shop_id,u.firstname FROM tbl_user_location  tsl 
            left join tbl_user u on tsl.userid=u.id 
            where tsl.id in ( select max(tsl1.id) as idnew from tbl_user_location tsl1 where date_format(tsl1.tdate, '%Y-%m-%d') = STR_TO_DATE('".$today."','%Y-%m-%d') group by tsl1.userid )
            ";
            $result1 = mysqli_query($this->local_connection,$sql);		
            $i = 0;
            $row_count = mysqli_num_rows($result1);
            if($row_count > 0){
                    while($row = mysqli_fetch_assoc($result1))
                    {
                            $records[$i] = $row;
                            $i++;
                    }
                    return $records;
            }else{
                    return 0;
            }
	}
        public function getCartonDetails($cartonids) {
       
        $carton_details_sql = "SELECT tdc.id,tdc.cartons_id,tdc.brand_id,tdc.cat_id,tdc.prod_id,tdc.prod_var_id,tdc.qnty,
                                                tdc.carton_added_date,tc.brand_name,tc.category_name,tc.product_name,tc.product_price,
                                                tc.product_variant1,tc.product_variant2
                                                FROM tbl_dcp_cartons tdc 
                                                left join pcatbrandvariant tc on tdc.prod_id=tc.product_id 
                                                and tdc.prod_var_id=tc.product_variant_id where cartons_id ='$cartonids' ";
        $carton_det_result = mysqli_query($this->local_connection, $carton_details_sql);
        //$row_orders_det_count = mysqli_num_rows($orders_det_result);
        if (mysqli_num_rows($carton_det_result) > 0) {
            while ($row_carton_det = mysqli_fetch_assoc($carton_det_result)) {
                $all_cartons['carton_details'][] = $row_carton_det;
            }
        }
        return $all_cartons;
    }
}
?>