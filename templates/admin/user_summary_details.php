<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/userManage.php";   
$userObj    =   new userManager($con,$conmain);
$user_type_title = $userObj->getUserTitle();
//$row = $userObj->getAllUsers(); 
$row = $userObj->getAllUsersView();
//print_r($row);
$colspan = "7";
if($_POST['user_type'] == 'salesperson'){
	$colspan = "9";	
}
if($_POST['user_type'] == 'Distributor' || $_POST['user_type'] == 'DeliveryPerson' || $_POST['user_type'] == 'DeliveryChannelPerson')
{
	$colspan = "8";	
}
?>
<? if($_POST["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? } ?>
<table 
	class="table table-striped table-bordered table-hover table-highlight table-checkable" 
	data-provide="datatable" 
	data-display-rows="10"
	data-info="true"
	data-search="true"
	data-length-change="true"
	data-paginate="true"
	id="sample_2">

<thead>
<tr>
	<td colspan="<?=$colspan;?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b><?=$user_type_title;?></b></h4></td>              
  </tr>
  <tr>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Name</th>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Email</th>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Mobile No.</th>
	<?php if($_POST['user_type'] == 'Distributor' || $_POST['user_type'] == 'DeliveryPerson' || $_POST['user_type'] == 'DeliveryChannelPerson'){ ?>
		<th data-filterable="false" data-sortable="true" data-direction="desc">Assign Superstockist</th>
	<?php } ?>
	<th data-filterable="false" data-sortable="true" data-direction="desc">City</th>
	<th data-filterable="false" data-sortable="true" data-direction="desc">State</th>
	<?php if($_POST['user_type'] == 'salesperson'){ ?>
		<th data-filterable="false" data-sortable="true" data-direction="desc">Taluka</th>
		<th data-filterable="false" data-sortable="true" data-direction="desc">Assign Superstockist</th>
	<?php } ?>
	<th data-filterable="false">Action</th>              
  </tr>
</thead>
<tbody>					
	<?php 
			foreach($row as $key => $val) 
			{ 
				$prod_qnty .= '<a onclick="showOrderDetails(\'' . $value['odid'] . '\',\'Order Details\')" title="Order Details">' . $value['product_quantity'];
			?>
				<tr class="odd gradeX">
				<td> 
				<a href="user_update.php?id=<?=$val['id'];?>&&u_type=<?=$val['user_type'];?>"><?=$val['firstname'];?></a></td>
				<td><?=$val['email'];?></td>
				<td><?=$val['mobile'];?></td>
				<?php 
				if($_POST['user_type'] == 'Distributor' || $_POST['user_type'] == 'DeliveryPerson' || $_POST['user_type'] == 'DeliveryChannelPerson')
					{ 
						?>
					<td>
						<?php echo $val['sstockist_name'];?>
					</td>
				<?php
				 }
				?>
				<td><?=$val['cityname'];?>				
						
						
					</td>
				<td><?=$val['statename'];?></td>
				<?php 
				if($_POST['user_type'] == 'salesperson')
				{ 
				?>
					<!-- <td>
						<?php
                           $subarea_ids = $val['subarea_ids'];                           
                           $count =explode(',', $subarea_ids);
                            $count1 = count($count);                 
									for ($i=0; $i <$count1 ; $i++) 
									{ 
									$count2 = $count[$i];
									$sql="SELECT subarea_id,subarea_name  FROM tbl_subarea where subarea_id ='".$count2."'";      
									$result = mysqli_query($con, $sql);
									while ($row = mysqli_fetch_array($result)) 
									{
									echo $row['subarea_name'].",";
									}  
									}                                                       
						?>
					</td> -->

<td>
						<?php
                           $suburb_ids = $val['suburb_ids'];                           
                           $count =explode(',', $suburb_ids);
                            $count1 = count($count);                 
									for ($i=0; $i <$count1 ; $i++) 
									{ 
									$count2 = $count[$i];
									$sql="SELECT id,suburbnm  FROM tbl_area where id ='".$count2."'";      
									$result = mysqli_query($con, $sql);
									while ($row = mysqli_fetch_array($result)) 
									{
									echo $row['suburbnm'].",";
									}  
									}                                                       
						?>
					</td>

					<td><?php echo $val['sstockist_name'];?></td>
				<?php 
			      }
				?>

					

				<td align='right'>

		           <a onclick="showOrderDetails('<?php echo $val['id']; ?>','<?php echo $val['user_type']; ?>');" >View</a>

					<a href="manageuser.php?utype=<?=$val['user_type'];?>&id=<?=$val['id'];?>">Delete</a>

				</td>									
				</tr>			
	<?php	} 
		if($_POST["actionType"]=="excel" &&  $row == 0) {
			echo "<tr><td>No matching records found</td></tr>";
		}
	?>	
			
</tbody>	
</table>
<script>
jQuery(document).ready(function() {    
   
   ComponentsPickers.init();
});

jQuery(document).ready(function() { 
	TableManaged.init();
});
$(document).ready(function() {
      var table = $('#sample_2').dataTable();
      // Perform a filter
      table.fnFilter('');
      // Remove all filtering
      //table.fnFilterClear();
	   
  });
</script>

<!-- END JAVASCRIPTS -->
<?
if($_POST["actionType"]=="excel") {
	if($row != 0){
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=Report_summary.xls");
		exit;
	}
} ?>
 