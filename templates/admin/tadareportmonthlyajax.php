<?php
include ("../../includes/config.php");
	$where="";
	switch($_SESSION[SESSION_PREFIX.'user_type']){
	case "Admin":				
			$where.=" ";
		break;
		case "Superstockist":			
			$where.=" AND u.sstockist_id='".$_SESSION[SESSION_PREFIX."user_id"]."' ";
		break;
		case "Distributor":
			$where.=" AND u.external_id='".$_SESSION[SESSION_PREFIX."user_id"]."' ";				
		break;
	}
extract($_POST);//$frmdate,$dropdownSalesPerson,$todate,$selTest
		//print_r($dropdownSalesPerson); exit;
		$condnsearch="";
		if($selTest!=5){
			$condnsearch.=" Where ";
				switch($selTest){
					
					case 3:
						if($todate!="")
							$todat=$todate;
						else
							$todat=date("d-m-Y");			
						
						$condnsearch.="  (date_format(tada.date_tada, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(tada.date_tada, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y')) ";
						//$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
					break;
					
					default:
						$condnsearch.=" ";
					break;
				}
			}else{
			$condnsearch.=" ";
		}
		
     $sql="SELECT u.firstname,u.id as spid,tada.* ,tmt.van_type
		FROM tbl_sp_tadabill tada  
		left join tbl_user  u on tada.userid=u.id
		left join tbl_mode_transe  tmt on tada.mode_of_transe=tmt.id ".$condnsearch." ".$where." order by date_tada ,firstname";
		//date_format(TUL.tdate, '%d-%m-%Y') = '".$frmdate."' ";
				$result = mysqli_query($con,$sql);
				$record_count=mysqli_num_rows($result);
	
				
?>
<? if($_GET["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? } ?>
<div id="loader_div1"></div>
<div class="portlet box blue-steel">
	<div class="portlet-title">
		<? if($_GET["actionType"]!="excel") { ?>
		<div class="caption"><i class="icon-puzzle"></i>Expense Report <?php echo "(".$frmdate."-".$todate.")";?></div>
		<?  if($record_count > 0) { ?>
		<button type="button" name="btnExcel" id="btnExcel" onclick="ExportToExcel();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
			&nbsp;
			<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
		
		<? } } ?>
	</div>
	
	<div class="portlet-body">
		<div class="table-responsive" id="dvtblResonsive">
			<table class="table table-bordered" id="report_table">
				<?php  if($record_count > 0) { ?>
				<thead>
					<tr>
						<th  valign="top" style="text-align:center" rowspan='2'><b>Date</b></th>
						<th valign="top" style="text-align:center" rowspan='2' ><b>Name</b></th>
						<th valign="top" style="text-align:center" rowspan='2'><b>Mode Of Transport/<br>Rate Per Km</b></th>
						<th  style="text-align:center" ><b>Actual </b></th>
						<th  style="text-align:center"><b>Google </b></th>
						<th  style="text-align:center" ><b>Food</b></th>
						<th  style="text-align:center"><b>Other</b></th>
						<th  style="text-align:center" colspan='2'><b>Total ( In Rs )</b></th>
						<th  valign="top" style="text-align:center" rowspan='2'><b>Expense Bill</b></th>
					</tr>
					<tr>
						
						<th  style="text-align:center" colspan='2'><b>Distance Travelled ( In Km )</b></th>
					
						<th  style="text-align:center" colspan='2'><b>Expenses ( In Rs )</b></th>
						
						<th  style="text-align:center"><b>Actual </b></th>
						<th  style="text-align:center"><b>Google </b></th>
						
					</tr>
				</thead>
				<tbody>
				<?php 
				$total_distance_covered=0;
				$total_google_distance=0;
				$total_food=0;
				$total_other=0;
				$total_actual_row=0;
				$total_google_row=0;
				
				while($row = mysqli_fetch_array($result)){ 
				$total_distance_covered=$total_distance_covered+$row['distance_covered'];
				$total_google_distance=$total_google_distance+$row['google_distance'];
				$total_food=$total_food+$row['food'];
				$total_other=$total_other+$row['other'];
				$total_actual_row=$total_actual_row+($row['Current_rate_mot']*$row['distance_covered'])+$row['food']+$row['other'];
				$total_google_row=$total_google_row+($row['Current_rate_mot']*$row['google_distance'])+$row['food']+$row['other'];
				?>
					<tr>
					
						<td><?php echo date('d-m-Y',strtotime($row["date_tada"]));?></td>
						<td><?php echo $row['firstname'];?></td>
						<td><?php echo $row['van_type']."/".$row['Current_rate_mot'];?></td>		
						<td align="right"><?php echo $row['distance_covered'];?></td>	
						<td align="right"><?php echo $row['google_distance'];?></td>
						<td align="right"><?php echo $row['food'];?></td>	
						<td align="right"><?php echo $row['other'];?></td>
						<td align="right"><?php $total1=($row['Current_rate_mot']*$row['distance_covered'])+$row['food']+$row['other'];
						echo $total1;?></td>
						<td align="right"><?php $total2=($row['Current_rate_mot']*$row['google_distance'])+$row['food']+$row['other'];
						echo $total2;?></td>
						<td>
							<?php
							$dir="../../uploads/".date('d-m-Y',strtotime($row["date_tada"]))."/".$row['spid']."/".$row["id"]."/";
						
								if (is_dir($dir)) {	
							$images = glob($dir."*.png");
							foreach($images as $key=>$image) {								
								echo '<img id="myImg'.date('dmY',strtotime($row["date_tada"])).''.$row['spid'].''.$key.'" onclick="changeItnew(this.id)" src="'.$image.'"  width="50px"/> ';
								
							} } else {  } ?>
						</td>
					</tr>
                                <?php	} ?>
					<tr>
						<td align="right" colspan="3"><b>Total</b></td>						
						<td align="right"><b><?php echo $total_distance_covered;?></b></td>	
						<td align="right"><b><?php echo $total_google_distance;?></b></td>
						<td align="right"><b><?php echo $total_food;?></b></td>	
						<td align="right"><b><?php echo $total_other;?></b></td>
						<td align="right"><b><?php echo $total_actual_row;?></b></td>
						<td align="right"><b><?php echo $total_google_row; ?></b></td>
						<td>
							
						</td>
					</tr>
								<?php }else{
					echo "<tr><td>No Record available.</td></tr>";
				}?>
				</tbody>
				
			</table>
		</div>
	</div>
</div> 