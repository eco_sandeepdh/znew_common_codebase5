<? 
include ("../../includes/config.php");
extract($_POST);

switch($_SESSION[SESSION_PREFIX.'user_type']){
	case "Admin":											
		$sql="SELECT VO.id AS variant_oder_id, VO.* , OA.* FROM `tbl_variant_order` AS VO 
		LEFT JOIN `tbl_order_app` AS OA ON VO.orderappid = OA.id 
		LEFT JOIN tbl_shops shops ON shops.id= VO.shopid
		where VO.product_varient_id = '".$product_varient_id."'   ";
		break;
	case "Superstockist":											
		$sql="SELECT VO.id AS variant_oder_id, VO.* , OA.* FROM `tbl_variant_order` AS VO 
		
		LEFT JOIN `tbl_order_app` AS OA ON VO.orderappid = OA.id 
		LEFT JOIN tbl_shops shops ON shops.id= VO.shopid
		
		where VO.product_varient_id = '".$product_varient_id."' AND VO.status='3'  AND OA.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'  ";
		break;
	case "Distributor":
		$sql="SELECT VO.id AS variant_oder_id, VO.* , OA.* FROM `tbl_variant_order` AS VO 
		
		LEFT JOIN `tbl_order_app` AS OA ON VO.orderappid = OA.id 
		LEFT JOIN tbl_shops shops ON shops.id= VO.shopid
		
		where VO.product_varient_id = '".$product_varient_id."' AND ( VO.status='2' OR VO.status='3' ) AND OA.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'  ";											
	break;
}


if($dropdownSalesPerson!="")
{
	$condition .= " AND OA.order_by = " . $dropdownSalesPerson;
} else if( $dropdownStockist!="") {
	$condition .= " AND OA.distributorid = " . $dropdownStockist;
} else if($cmbSuperStockist!="") {
	$condition .= " AND OA.superstockistid = " . $cmbSuperStockist;
}

if($dropdownshops !="")
{
	$condition .= " AND OA.shop_id = " . $dropdownshops;
}
if($subarea !="") {
	$condition .= " AND shops.subarea_id = " . $subarea;
}
if($dropdownSuburbs !="")
{
	$condition .= " AND shops.suburbid = " . $dropdownSuburbs;
}
if($dropdownCity !="")
{
	$condition .= " AND shops.city = " . $dropdownCity;
}
if($dropdownState !="")
{
	$condition .= " AND shops.state = " . $dropdownState;
}

$sql .= $condition . " ORDER BY VO.id desc";

?>
<form class="form-horizontal" role="form" name="form" method="post" action="">
<div class="clearfix"></div>
<table class="table table-striped table-bordered table-hover" id="sample_2">
<thead>
<tr> 
<th>Shop Name
</th>
<th>
Sales Person Name
</th>	
<th>
Order Date
</th>
<th>
Unit Price <i aria-hidden='true' class='fa fa-inr'></i>
</th>
<th>
Quantity
</th>
<th>
Total Price <i aria-hidden='true' class='fa fa-inr'></i>
</th>
<th>
</th>	
</tr>
</thead>
<tbody>
<?php							

$result1 = mysqli_query($con,$sql);
if($result1>0)	
{
	while($row = mysqli_fetch_array($result1)) 
	{
		//get distributor
		if($row['distributorid']==0)		
			$distributornm="";
		else
			$distributornm=$row['distributornm'];

		if($row['superstockistid']==0)		
			$superstockistnm="";
		else
			$superstockistnm=$row['superstockistnm'];
	?>
	<tr class="odd gradeX">	
	<?php	 
		$total_cost = $row['variantunit'] * $row['totalcost'];
		$display_icon = '';
		if($row['campaign_sale_type'] == 'free')
			$display_icon = '<span style="float: left"><img src="../../assets/global/img/free-icon.png" title="Free Product"></span>'; ?>
		<td><?=$row['shopnme'];?></td>
		<td><?=$row['salespfullnm'];?></td>										    
		<td><?=date('d-m-Y H:i:s',strtotime($row['order_date']));?></td>
		<td align="right"><?=number_format($row['totalcost'],2, '.', '');?></td>
		<td align=""><?=$display_icon;?><span style="float: right"><?=$row['variantunit'];?></span></td>
		<td align="right"><?=number_format($total_cost,2, '.', '');?></td>												
		<td>
		<a onclick="view_product_order(<?=$row['variant_oder_id']; ?>)">view</a>
		</td>
		</tr>
	<?
	}
}
?>							
</tbody>
</table>
</form>
