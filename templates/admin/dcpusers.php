<!-- BEGIN HEADER -->
<?php
include "../includes/grid_header.php";
include "../includes/userManage.php";
$userObj = new userManager($con, $conmain);
//$userObj->migration_sp_sstockist(); exit;
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "ManageSupplyChain";
        $activeMenu = "DeliveryChannel";
        include "../includes/sidebar.php"
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                <!-- /.modal -->
                <h3 class="page-title">
                    Delivery channel Person
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Delivery channel Person</a>
                        </li>
                    </ul>

                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">


                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                            <div class="caption">
                                Delivery channel Persons Listing
                            </div>
                                <a href="dcpuser-add.php" class="btn btn-sm btn-default pull-right mt5">
                                    Add Delivery channel Person
                                </a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">

                                <table class="table table-striped table-bordered table-hover" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th width="15%">
                                                Name
                                            </th>	
                                           							
                                            <th width="20%">
                                                Email
                                            </th>
                                            <th width="10%">
                                                Mobile Number
                                            </th>
                                            <th width="10%">
                                                Taluka
                                            </th>    
                                            <?php if ($_SESSION[SESSION_PREFIX . 'user_type'] != 'Distributor') { ?>
                                                <th width="10%">
                                                    Action
                                                </th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        switch ($_SESSION[SESSION_PREFIX . 'user_type']) {
                                            case "Admin":
                                                $user_type = "DeliveryChannelPerson";
                                                $external_id = "";
                                                break;
                                            case "Superstockist":
                                                $user_type = "DeliveryChannelPerson";
                                                $external_id = $_SESSION[SESSION_PREFIX . 'user_id'];
                                                break;
                                           
                                        }

                                        $result1 = $userObj->getAllLocalUserDetails($user_type, $external_id);
                                        while ($row = mysqli_fetch_array($result1)) {
                                            $row_count = 0;
                                            $external_id = $row['external_id'];
                                            $assign_stockist = "";
                                            if ($external_id != '') {
                                                $sql_suburb = "SELECT GROUP_CONCAT(suburb_ids) AS suburb_ids FROM tbl_user_working_area where user_id IN (" . $external_id . ")";
                                                $result_suburb = mysqli_query($con, $sql_suburb);
                                                $row_count = mysqli_num_rows($result_suburb);
                                                $row_suburs = array();
                                                if ($row_count > 0) {
                                                    $row_suburs = mysqli_fetch_assoc($result_suburb); //print"<pre>";print_r($row_suburs);	
                                                }
                                                $sql_assign_stockist = "SELECT GROUP_CONCAT(firstname) AS assign_stockist FROM tbl_user WHERE id IN (" . $external_id . ")";
                                                $result_assign_stockist = mysqli_query($con, $sql_assign_stockist);
                                                $row_count_assign_stockist = mysqli_num_rows($result_assign_stockist);
                                                $rec_assign_stockist = mysqli_fetch_assoc($result_assign_stockist);
                                                //print"<pre>"; print_r($rec_assign_stockist);
                                                $assign_stockist = str_replace(',', ', ', $rec_assign_stockist['assign_stockist']);
                                            }
                                            echo '<tr class="odd gradeX">
								<td>
									 <a href="dcpusers1.php?id=' . $row['id'] . '">' . fnStringToHTML($row['firstname']) . '</a>
								</td>';

                                            echo '<td>' . $row['email'] . '</td>
                                <td>' . $row['mobile'] . '</td>
                                <td>';
                                            if (!empty($row_suburs['suburb_ids']) && $row_count > 0) {
                                                $suburbs = str_replace(',,', ',', $row_suburs['suburb_ids']);
                                                $suburbs = rtrim($suburbs, ",");
                                                $suburbs = ltrim($suburbs, ",");
                                                $sql_s_name = "SELECT GROUP_CONCAT(suburbnm) AS all_suburb FROM tbl_area where id IN(" . $suburbs . ")";
                                                $result_s_name = mysqli_query($con, $sql_s_name);
                                                $suburb_count = mysqli_num_rows($result_s_name);
                                                $row_suburb = mysqli_fetch_assoc($result_s_name);
                                                echo $row_suburb['all_suburb'];
                                            } else {
                                                echo '-';
                                            }
                                            echo '</td>';
                                            ?>
                                        <div id="stockist_dropdown_<?= $row['id']; ?>" style="display:none;"> 
                                            <select name="assign[]" multiple class="form-control">				 
                                                <?php
                                                $user_type = "Distributor";
                                                $parentid = $row['sstockist_id'];
                                                $result_stockist = $userObj->getLocalUserDetailsByUserType($user_type, $parentid);
                                                if ($result_stockist != 0) {
                                                    while ($row_stockist = mysqli_fetch_array($result_stockist)) {
                                                        $assign_id = $row_stockist['id'];
                                                        $external_ids = explode(',', $row['external_id']);
                                                        if (in_array($assign_id, $external_ids))
                                                            $sel = "SELECTED";
                                                        else
                                                            $sel = "";
                                                        echo "<option value='$assign_id' $sel>" . fnStringToHTML($row_stockist['firstname']) . "</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <?php
                                        if ($_SESSION[SESSION_PREFIX . 'user_type'] != 'Distributor') {
                                            $assign_link = "-";
                                            if ($result_stockist != 0) {
                                                $assign_link = '<a onclick="javascript: assign_stockist(' . $row['id'] . ')">Assign</a> ';
                                            }
                                            if ($_SESSION[SESSION_PREFIX . 'user_type'] == "Admin") {
                                                $assign_link .= ' / <a href="manageuser.php?utype=DeliveryChannelPerson&id=' . $row['id'] . '">Delete</a>';
                                            }

                                            echo '<td>' . $assign_link . '</td>';
                                        }
                                        echo '</tr>';
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>



                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php" ?>
<!-- END FOOTER -->

</body>
<!-- END BODY -->
</html>